### Création d'un site hugo (n'importe où):
Cette commande va créer toute l'arborescence du site.
> hugo new site [NOM_DU_SITE] 

### Installation d'un theme:
Telecharger le fichier ".zip" sur git,
et l'installer dans le dossier "theme".

## Ouvrir le site dans le navigateur
Revenir à l'emplacement principal,
> hugo server pour le voir en mode publié
(> hugo server -D  pour voir le site en mode non publié)

### Generer le site
> hugo

### Lancement du exampleSite
> env HUGO_THEMESDIR="../.." hugo server  en ligne de commande,

### Création du nouveau site à partir du exampleSite
Copier le fichier exampleSite/config.toml à la racine.
Copier les dossiers présents dans exampleSite à la racine.

### Ajouter un sous-module
> git submodule add https://gitlab.com/quentin-garraud/hugo-template-one-page one-page 
`

### Clonage du projet directement avec le sous-module
>  git clone --recursive git@gitlab.com:quentin-garraud/[ LINK ] 
`