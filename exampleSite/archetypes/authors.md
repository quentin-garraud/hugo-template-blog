---
title: "{{ replace .Name "-" " " | title }}"
name: " "
image: "/images/authors/ "

socials:
  - social: "twitter"
    profil: ""
  - social: "instagram"
    profil: ""
  - social: "linkedin"
    profil: "in/"
---
