---
title: "Alexandre"
name: "Alexandre LEMERLE"
image: "/images/authors/alexandre.jpeg"

socials:
  - social: "facebook"
    profil: "lemerlealexandre"
  - social: "instagram"
    profil: "alexandre_lemerle"
  - social: "github"
    profil: "in/lemerle"
---
Gummi bears tootsie roll dragée cake tootsie roll chupa chups bonbon candy canes cake. Pudding caramels lemon drops jujubes danish chocolate bar. Marzipan jelly-o chocolate bar pie tiramisu danish chocolate bar cheesecake. Tiramisu wafer tootsie roll. Marshmallow pastry toffee jelly-o croissant apple pie. Brownie chupa chups candy canes. Jelly bonbon biscuit chocolate bar donut. Carrot cake candy canes apple pie sugar plum cake. Biscuit lemon drops dragée dessert sugar plum. Apple pie biscuit carrot cake marzipan gummi bears ice cream tart pie.

