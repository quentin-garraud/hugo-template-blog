---
title: "Cecilia"
name: "Cecilia Rua"
image: "/images/authors/cecilia.jpeg"

socials:
  - social: "twitter"
    profil: "cecilia-rua"
  - social: "instagram"
    profil: "ruacecilia"
  - social: "linkedin"
    profil: "in/ceciliarua"
---
Ice cream marshmallow brownie topping marshmallow halvah. Sweet roll sweet roll tootsie roll tart sesame snaps jujubes jelly dragée bear claw. Chocolate cake icing croissant cupcake cake gingerbread lollipop pie dragée. Oat cake jujubes toffee cotton candy jelly-o cupcake lollipop caramels. Marzipan jelly beans danish. Candy dragée jujubes. Dragée toffee chupa chups candy canes chocolate bar oat cake sweet. Tiramisu cupcake cake ice cream lemon drops bonbon chocolate bar. Pastry dragée jelly candy canes liquorice ice cream. Donut chocolate sugar plum jelly beans jelly-o powder.

Gingerbread candy biscuit soufflé fruitcake apple pie tart. Cheesecake cake jujubes chocolate muffin soufflé jelly. Pastry ice cream lollipop sugar plum sweet chocolate bar apple pie. Cheesecake gingerbread lemon drops muffin. Liquorice tiramisu bear claw. Croissant gummi bears cookie dessert lemon drops. Cotton candy soufflé jelly marzipan gingerbread. Sweet lemon drops tart tiramisu dragée jelly-o.