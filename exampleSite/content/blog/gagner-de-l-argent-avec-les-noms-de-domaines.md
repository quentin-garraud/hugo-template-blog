---
title: "Revente de nom de domaine : comment gagner de l’argent avec les noms de domaines ?"
date: 2020-07-29T09:38:27+02:00
draft: false
categories: ["nomdedomaine", "argent", "TLD"]
tags: ["Nouveaux TLD", "TLDs"]
image: "https://www.ionos.fr/digitalguide/fileadmin/DigitalGuide/Teaser/geld-verdienen-t_01.jpg"
summary: 'Si vous avez choisi un bon nom de domaine, il peut valoir beaucoup d’argent aujourd’hui. Peu importe combien vous avez dépensé pour le site Web qui utilise le nom de domaine. En principe, vous n’avez même pas besoin d’un site pour pouvoir acheter et vendre un nom de domaine. L’adresse en elle-même simplement a de la valeur. Si vous possédez un domaine que d’autres envient énormément, vous pouvez peut-être même faire fortune avec ! Mais comment ? Et que faut-il faire pour vendre un nom de domaine ?'
authors: ["quentin"]
---

Si vous avez choisi un bon nom de domaine, il peut valoir beaucoup d’argent aujourd’hui. Peu importe combien vous avez dépensé pour le site Web qui utilise le nom de domaine. En principe, vous n’avez même pas besoin d’un site pour pouvoir acheter et vendre un nom de domaine. L’adresse en elle-même simplement a de la valeur. Si vous possédez un domaine que d’autres envient énormément, vous pouvez peut-être même faire fortune avec ! Mais comment ? Et que faut-il faire pour vendre un nom de domaine ?

### Déterminer la valeur d’un nom de domaine

Si vous voulez profiter d’une bonne revente de nom de domaine, vous devez d’abord **estimer la valeur de votre nom de domaine**. Les premiers indices de sa valeur se trouvent dans l’URL. Les noms de domaines de premier niveau tels que *.com* ou *.fr* sont beaucoup plus demandés que les nouveaux TLDs tels que .beauty. Les domaines courts et concis sont également plus populaires que les domaines composés de plusieurs mots. Les domaines qui se composent de plusieurs parties et qui contiennent des mots assez inhabituels sont beaucoup moins susceptibles de trouver preneurs et se vendent plutôt à des prix très bas.

Pour vous faire une idée du marché, vous pouvez utiliser des outils en ligne comme Namebio. Ils indiquent les prix de vente de noms de domaine similaires. De cette façon, vous pouvez déterminer approximativement le prix que vous pouvez demander pour votre nom de domaine. Bien sûr, vous voudrez certainement tirer le meilleur parti de votre adresse web, mais un prix trop élevé peut effrayer les acheteurs. Trouvez donc un bon compromis.

### Déterminer le canal de vente

Il y a différentes façons de vendre un nom de domaine. Par exemple, vous pouvez placer un avis directement sur la page d’accueil de votre nom de domaine : une sorte de panneau « à vendre », pour ainsi dire. Les personnes intéressées peuvent alors vous contacter directement pour discuter de la vente du nom de domaine. Certains traders de nom de domaines (que l’on appelle les « domainers ») ont du succès sur eBay. La plateforme de vente aux enchères offre en effet un espace de vente optimal à cela. Si vous décidez de négocier des noms de domaines sur eBay, le principe est le même que pour les autres produits de la plateforme.

Mais il y a aussi des sites que vous pouvez utiliser spécifiquement pour acheter et vendre des noms de domaines. Des plateformes comme Sedo ou Flippa offrent des marchés où vous pouvez proposer votre domaine. Si vous voulez faire du trading de noms de domaine, c’est-à-dire non seulement vendre un domaine inutilisé, mais aussi rechercher des domaines de valeur afin de les revendre plus tard ; ces plateformes sont très importantes.

### Saisir ses informations : le registre Whois

En principe, chaque propriétaire d’un domaine doit fournir ses coordonnées dans Whois. Il s’agit d’une entrée de registre qui, par défaut, est accessible de manière publique. Comme les entrées Whois qui peuvent être consultées sont non seulement discutables en vertu de la loi sur la protection des données, mais provoquent également régulièrement des attaques de spam, ces informations sont souvent gardées secrètes. Mais ce n’est pas le cas avec le trading de noms de domaine : si vous voulez vendre votre nom de domaine de manière la plus lucrative possible, vous devez rendre vos coordonnées visibles. Les acheteurs intéressés, par exemple les commerçants, peuvent alors facilement vous contacter.

### Liste des noms de domaine

Si vous avez choisi une plateforme sur laquelle vous voulez proposer votre nom de domaine, il vous faudra alors créer une entrée. Tout comme pour les autres ventes, quelques règles sont à suivre : si vous mentionnez simplement le nom de domaine et son prix, vous n’attirerez probablement pas énormément de clients. Ce n’est pas assez attrayant. Si vous fournissez des renseignements supplémentaires cependant, vous serez plus enclin à persuader les parties intéressées de concrétiser leur achat.

Au premier abord, cela semble absurde, mais un domaine peut aussi être doté d’une description de produit. Par exemple, insérez des informations sur le classement du nom de domaine et précisez pour quel secteur l’URL il peut convenir. Même une image peut être insérée : un traitement graphique de votre nom de domaine ou une capture d’écran de votre ancien site peut inciter les visiteurs à regarder de plus près votre offre. Si possible, vous pouvez également examiner de plus près la valeur actuelle ou future du domaine et justifier un prix demandé.

### Le paiement du nom de domaine

Sur de nombreuses places de marché, vous pouvez choisir entre deux méthodes : vous pouvez spécifier un prix fixe ou opter pour une enchère. Les deux ont leurs avantages : un prix fixe est plus attrayant pour de nombreux acheteurs parce ils savent tout de suite sur quoi ils s’engagent. Mais beaucoup de vendeurs préfèrent les enchères parce qu’ils pensent qu’ils peuvent faire de meilleures affaires. Avec les enchères, vous pouvez en effet espérer que la vente dépasse vos espérances si votre nom de domaine a du potentiel.
Vous devriez également penser au processus de paiement concret, car la vente d’un domaine fonctionne différemment de l’achat de vêtements sur Internet. Il est nécessaire pour vous comme pour l’acheteur d’avoir des garanties. De nombreuses places de marché offrent donc un service pour agir en tant qu’intermédiaire. Si vous ne vendez pas votre domaine sur une place de marché, il est recommandé d’avoir tout de même recours à des services intermédiaires afin qu’il n’y ait pas de litige par la suite.


### Transfert du nom de domaine

Une fois que vous avez vendu votre nom domaine, vous devez le transférer à l’acheteur. Cependant, le fonctionnement de ce processus dépend de votre fournisseur de nom de domaine, car c’est également là que sont gérés les droits d’accès. Finalement, le nouveau propriétaire sera inscrit dans le registre, et la vente sera ainsi conclue. Si vous avez vendu votre nom de domaine par l’intermédiaire d’une place de marché spécialisée, cette dernière vous assistera en principe pour effectuer le transfert du nom de domaine.

source : https://www.ionos.fr/digitalguide/domaines/actualites-sur-le-domaine/peut-on-gagner-de-largent-avec-les-noms-de-domaine/