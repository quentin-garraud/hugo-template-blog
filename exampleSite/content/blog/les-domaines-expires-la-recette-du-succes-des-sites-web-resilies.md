---
title: "La réussite des noms de domaines expirés américains"
date: 2020-07-29T09:48:59+02:00
draft: false
categories: ["nomdedomaine"]
tags: ["E-Commerce", "Facebook", "Navigateur"]
image: "https://www.ionos.fr/digitalguide/fileadmin/DigitalGuide/Teaser/expired-domain-t.jpg"
summary: 'Pour ce qui est du marketing en ligne, les termes « expired domain » (domaine expiré) et « dropped domain » (domaine abandonné) sont encore très courants. Il s’agit des adresses de sites web créés par une entreprise ou une personne privée. À l’expiration de la durée du contrat, les sites en question ne sont pas renouvelés et sont résiliés. Ensuite, il est possible de les enregistrer à nouveau. Toutefois, grâce aux hébergeurs web, les exploitants de sites web sécurisent presque toujours leurs noms de domaine de sorte que ces derniers restent enregistrés pour une période plus longue que la période minimale d’un an.'
---

Pour ce qui est du marketing en ligne, les termes « expired domain » (domaine expiré) et « dropped domain » (domaine abandonné) sont encore très courants. Il s’agit des adresses de sites web créés par une entreprise ou une personne privée. À l’expiration de la durée du contrat, les sites en question ne sont pas renouvelés et sont résiliés. Ensuite, il est possible de les enregistrer à nouveau. Toutefois, grâce aux hébergeurs web, les exploitants de sites web sécurisent presque toujours leurs noms de domaine de sorte que ces derniers restent enregistrés pour une période plus longue que la période minimale d’un an.

Il existe beaucoup de raisons pour lesquelles les domaines sont résiliés. Par exemple, si une entreprise a fermé ou si un projet commercial a été terminé, le portfolio du domaine est supprimé dans sa totalité. Pour ces noms des domaines expirés qui deviennent à nouveau disponible, il existe cependant plusieurs fournisseurs qui les achètent et les font revivre.

### Trouver les noms des domaines expirés : qu’est-ce qui les rend si intéressants ?

Ce qui rend un domaine expiré particulièrement attrayant, c’est son utilité dans l’optimisation des moteurs de recherche off-page (OMR ou SEO en anglais). Dans le meilleur des cas, il est possible de prendre en charge et utiliser la structure des backlinks disponible. Un véritable gain de temps pour ceux qui auraient autrement dû développer continuellement une structure de liens entrants. En règle générale, la valeur d'un site Web croît sur plusieurs années de manière organique. Avec le classement dans les moteurs de recherche et les backlinks, la valeur du domaine augmente également. L'achat d'un domaine expiré est une bonne occasion pour les exploitants de sites Web de raccourcir ce long et fastidieux travail.

Il y a aussi des risques potentiels liés à l’utilisation des domaines expirés. L’achat en vaut la peine seulement si le site web apporte du trafic et jouit d’une bonne réputation. Dans le pire des cas, vous récupérez l’héritage du domaine abandonné : par exemple, des problèmes avec des bots, spams, et autres avertissements Google. Il est donc très important d'obtenir à l'avance des informations détaillées sur le domaine en question.

### Le cas de Cameron Harris : de l’argent facile via des fakes news sur un domaine expiré

Le New York Times a fourni des informations sur Cameron Harris, un diplômé universitaire de sciences politiques qui a été en mesure de créer une entreprise lucrative avec l’acquisition d'un domaine expiré. Son idée a aussi été considérée comme un « chef-d'œuvre » en termes de « fausses nouvelles » (les fameuses fake news) par le journal en question. Après avoir terminé ses études, Harris était à la recherche d'un modèle commercial lucratif et a aussi eu l'idée d'effectuer une « expérience sociologique », comme il l'a décrit.

Au début, Harris a mis quelques articles en ligne, avec lesquels il n'a obtenu qu'une résonnance limitée et un faible nombre de clics. Après avoir accusé Hillary Clinton d'avoir qualifié la fusillade du gorille Harambe d’acte raciste, il a reçu les premières réactions à ses articles. Harris s'est ensuite concentré sur des questions politiques, car elles ont attiré beaucoup d'attention. Entre autres, il a accusé Bill Clinton de participation à un réseau de pédophilie et a déclaré l’intention de son épouse de demander le divorce.

### Comment a-t-il gagné de l’argent avec un domaine expiré ?

Harris expliquait que, en utilisant le fournisseur web ExpiredDomains.net, il a trouvé et acheté (pour la somme de 5 dollars américains) le nom de domaine expiré «ChristianTimesNewspaper.com», qu’il utilisait ensuite pour son «expérience sociologique». Selon ses dires, le titre de l'URL ne devait fournir que la crédibilité nécessaire pour le site web en question. Harris a publié plusieurs articles sur son domaine nouvellement acquis, se concentrant thématiquement sur la campagne électorale américaine entre Hillary Clinton et Donald Trump.

Parmi ses articles les plus populaires, Harris annonçait ce que les théoriciens du complot suspectaient depuis longtemps : une dizaine de milliers de votes pour Clinton trouvée dans un entrepôt en Ohio. « Le cas est en train d’être déterminé par la police », disait Harris dans son article accompagné par une image d’un magasinier qui tient un grand coffre contenant prétendument des bulletins de vote manipulés. Harris accompagnait cette image d’un article sur un complot allégué, qui influençait le résultat des élections. Les bulletins de vote en question seraient donc introduits clandestinement aux votes dûment remplis le jour du dépouillement afin que Clinton puisse remporter la victoire de manière illicite. Pour faire passer le mot, Harris créait quelques faux profils Facebook où il partageait ses articles. Et ça a marché : l'histoire de la fraude électorale s'est répandue de manière virale. Rien qu'avec cet article, Harris a gagné 5 000 $ grâce à Google AdSense, car de nombreuses personnes ont cliqué sur cet article monétisé et l’ont partagé.

Curieusement, l'article a reçu 6 millions de clics et était partagé même par Donald Trump. Par conséquent, le Président actuel des États-Unis utilisait des fausses nouvelles pour attaquer son opposant politique, dénigrer les médias et contester la légitimité du gouvernement d’Obama : quelque chose qu’il faisait déjà en tant que star de la télé-réalité jusqu’à sa montée au plus haut niveau de l’État américain.

### Quelle motivation l’a poussé à publier des Fake News sur des domaines expirés ?

D'autre part, Harris a nié toute motivation politique derrière ces fausses nouvelles : il ne s'agissait que d’un intérêt pour un profit maximal pouvant être atteint avec les thèmes politiques. Lorsqu'on lui demandait s'il avait des sentiments de culpabilité après avoir diffusé des fausses informations au sujet des candidats présidentiels, Harris répondait que ses articles n’ont guère influencé l’état des choses, parce que, dans le monde de la politique composé généralement d'exagérations et de demi-vérités, la diffusion de telles informations est une pratique courante depuis longtemps.

Harris a également réaffirmé que, afin de favoriser Hillary Clinton dans ses articles, il aurait été prêt à s’opposer à Trump si cette tactique avait été plus profitable. Cependant, les supporteurs de Trump se sont avérés être beaucoup plus impulsifs que ceux de Clinton, parce qu’ils ont partagé les articles anti-Clinton de manière bien plus spontanée.

Harris réitérait son sentiment républicain, mais finalement, selon ses propres mots, il ne s’agissait que des 20 000 dollars qu’il gagnait grâce à Google Ads sur ses sites web. Cependant, après que ces manipulations aient été révélées, Google a rapidement privé Harris de revenus publicitaires.

Harris a aussi fait une erreur coûteuse : il a décidé d'attendre. Quelques jours après l'élection, Google annonçait qu'il ne placerait plus des annonces sur les sites porteurs de fake news. Ensuite, quelques jours plus tard, les annonces sur les domaines de Harris ont disparu. Un expert lui expliqua que ses domaines étaient désormais essentiellement sans valeur.

Cependant, tout n’a pas été perdu. Harris mettait en place un pop-up sur le domaine réactivé, qui invitait les visiteurs à devenir membres d’un collectif fictif "Stop the Steal" ("Arrête l’Arnaque"). Les utilisateurs découvraient comment Clinton manipule les élections et comment l’arrêter. De cette façon, Harris rassemblait 24 000 adresses email. Cependant, selon lui, l’étape suivante n’était pas encore claire.

source : https://www.ionos.fr/digitalguide/domaines/actualites-sur-le-domaine/les-domaines-expires-la-recette-du-succes-des-sites-web-resilies/