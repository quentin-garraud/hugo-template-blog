---
title: "Enchères record pour le .web, vendu à 135 millions de dollars"
date: 2020-07-29T09:55:06+02:00
draft: false
categories: ["nomdedomaine", "argent"]
tags: ["Nouveaux TLD", "TLDs"]
image: "https://www.ionos.fr/digitalguide/fileadmin/DigitalGuide/Teaser/domainendungen-t.jpg"

summary: 'Le verdict est tombé le 27 juillet 2016 : le Top-Level-Domain générique.web a été vendu aux enchères à la société Nu Dot Co pour la somme de 135 millions de dollars. L’entreprise américaine a remporté l’enchère pour exploiter le nom de domaine convoité par huit candidats et peut désormais vendre et louer cette extension de domaine. '
---
Le verdict est tombé le 27 juillet 2016 : le Top-Level-Domain générique.web a été vendu aux enchères à la société Nu Dot Co pour la somme de 135 millions de dollars. L’entreprise américaine a remporté l’enchère pour exploiter le nom de domaine convoité par huit candidats et peut désormais vendre et louer cette extension de domaine. 

C’est dans le cadre du programme concernant les nouveaux gTLD (domaines de premier niveau génériques) qu’une vente aux enchères a été organisée par le fournisseur de services d'enchères agréé par l'ICANN (Internet Corporation for Assigned Names and Numbers). L’objectif était de régler le litige qui avait opposé les candidats pour l’achat du gTLD .shop en janvier 2016.

L’extension de nom de domaine .web est considérée comme le successeur du très populaire Top-Level-Domain .com, qui est utilisé depuis de nombreuses années. Une majorité des noms de domaine convoités ont déjà été attribués en .com et de nombreuses sociétés souhaitaient désormais être gestionnaires du .web.

L’ICANN est donc à l’origine de cette vente aux enchères sans égal dans le domaine du Web. Le montant de l’enchère est un véritable record, étant sept fois supérieur à celui de l’acquisition d’un Top-Level-Domain usuel. Le record précédent d’une enchère était de de 41,5 millions de dollars pour l’extension e-commerce .shop qui a été achetée par l’entreprise japonaise GMO Registry. 

### Une vente aux enchères houleuse

La société Nu Dot Co était le seul candidat défavorable à une enchère privée. Dans ce cas de figure, la procédure de dernier recours pour les enchères privées prévoit qu’une somme identique au montant versé par l’acquéreur du gTLD est redistribuée équitablement par la suite entre les autres candidats, en guise de compensation (qui aurait donc été à hauteur de 135 millions de dollars).

Comme il y a eu des désaccords entre les candidats, la somme de la compensation est revenue en totalité à l’ICANN.

Les deux sociétés candidates Donuts et Radix ont décidé d’enclencher une procédure d’appel, dans l’espoir de retarder la vente aux enchères. Le recours en justice devant les tribunaux californiens était basé sur des soupçons concernant Nu Dot Co qui aurait collaboré avec l’entreprise américaine Verisign (qui est déjà propriétaire des extensions .net et .com). Nu Dot Co est soupçonné d’être une société vitrine et un des associés de l’entreprise l’aurait quitté pour Verisign afin de financer l’acquisition du .web.

Le recours à la voie juridique a été refusé par l’ICANN en l’espace de quatre jours seulement, bien que ce type de procédures prenne habituellement des mois. La société Donuts est, entre autres, propriétaire et gestionnaire des nouvelles extensions .business et .company. L’entreprise reproche avant tout à l’ICANN d’avoir enfreint sa propre politique d’entreprise et d’avoir fait preuve de concurrence déloyale, arguments qui n’ont pas été retenus par le tribunal californien.

### Qui se cache derrière Nu Dot Co?

Depuis le début de l’affaire juridique, l’entreprise américaine Verisign a officiellement reconnu être derrière l’offre de Nu Dot Co et avoir financé l’argent nécessaire à l’enchère. Ce fournisseur de noms de domaines est connu pour gérer et louer les extensions en.com et en .net. Maintenant que Verisign est également propriétaire de l’extension en .web, l’entreprise peut continuer à étendre son influence sur le marché.