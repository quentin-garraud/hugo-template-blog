---
title: "Achat Nom De Domaine"
date: 2020-07-29T15:20:19+02:00
draft: false
type: "page"
url: "/achat-nom-de-domaine/"
image: "https://solidnames.fr/wp-content/uploads/2020/03/achat-nom-domaine-email.jpg"
---
L’achat d’un nom de domaine se fait en plusieurs étapes. En premier lieu, vous choisissez votre nom par exemple, le nom de votre entreprise ou de votre marque enregistrée à l’INPI. En second lieu, vous sélectionnez l’extension internet associée (ex : .COM, .FR).

Au niveau mondial, l’ICANN attribue des extensions internet à des registres publics ou privés. Des extensions génériques, géographiques mais aussi des nouvelles extensions internet existent.

Ensuite, vous vérifiez la disponibilité du nouveau domaine trouvé pour votre adresse internet

Votre domaine idéal est déjà pris (c’est souvent le cas pour un domaine générique comme duo.fr) ? Dans ce cas, son rachat permet d’obtenir le nom de domaine. Une autre possibilité est de trouver un nom de domaine libre ne correspondant pas à une marque déposée.

Avoir un nom distinctif pouvant faire l’objet d’un dépôt de marque est recommandé pour réserver un nom de domaine.

### Bureau d’enregistrement de noms de domaine (« registrar »)

L’achat d’un nom de domaine se fait ensuite auprès d’un bureau d’enregistrement (« registrar » en anglais) accrédité par le registre de l’extension internet.

En France, c’est l’AFNIC, qui accrédite les bureaux d’enregistrement autorisés à proposer l’achat de noms de domaine en .FR. Comme on pouvait s’y attendre, Solidnames est un « registrar » accrédité par l’AFNIC.

Les bureaux d’enregistrements de noms de domaine sont des intermédiaires entre les titulaires et les registres. Les plus connus en France sont Gandi, OVH (également hébergeur), Ionos by 1&1, Amen, LWS, ou Viaduc.
Au niveau international, les « registrars » comptant le plus de noms de domaine déposés sont Go Daddy, NameCheap, Tucows, eNom ou Network Solutions. Ces registraires grands publics proposent souvent des noms de domaine pas chers.

A contrario, ils existent des bureaux d’enregistrements orientés vers les entreprises (exemples : IpTwins, ProDomaines, SafeBrands) et les grands comptes (ex : CSC, MarkMonitor, Nameshield).
Si vous n’êtes pas satisfaits de votre « registrar », le transfert de domaine vers un autre prestataire est possible. Le transfert d’un nom de domaine se fait via un code d’autorisation. Ce AUTHCODE est souvent disponible dans votre espace client.

